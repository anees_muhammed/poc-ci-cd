<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Mail\NotifyTimeLogMail;
use Mail;
use Carbon\Carbon;
use DB;
use App\Models\Team;
use App\Models\UserHour;
use App\Models\User;

class ConsolidatedTimeLogReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:timeLogReport';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send consolidated time log report of employees who have logged lesser than 6 hrs per day';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $date = Carbon::now()->format('Y-m-d');
        \Artisan::call('fetch:timeLog', ['date' => $date]);
        $sendTo = 'neel@whiterabbit.group';
        $sendMail = false;
        $allUserIds = User::pluck('id')->toArray();
        $completeLoggedUserIds = UserHour::whereDate('date_of_log', $date)->where('total_hours', '>=', 6)->orderBy('total_hours')->pluck('user_id')->toArray();
        $userIds = array_diff($allUserIds, $completeLoggedUserIds);
        $teams = Team::where('display_time_logs', 1)->with(['users' => function ($q) use ($userIds) {
            return $q->whereIn('users.id', $userIds);
        }])->get();
        foreach ($teams as $team) {
            if (!empty($team->users)) {
                foreach ($team->users as $user) {
                    $timeLog = UserHour::where('user_id', $user->id)->whereDate('date_of_log', $date)->first();
                    $user->total_hours = $timeLog ? $timeLog->total_hours : 0;
                }
                $team->users = $team->users->sortBy('total_hours');
                $sendMail = true;
            }
        }
        if ($sendMail) {
            Mail::to($sendTo)->send(new NotifyTimeLogMail($teams, $date));
        }
    }
}
