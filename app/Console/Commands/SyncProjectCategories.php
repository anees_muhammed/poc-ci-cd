<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use PortalAPIHelper;

use App\Models\Category;
use App\Models\User;

class SyncProjectCategories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:projectCategories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync project categories';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $portal = new PortalAPIHelper(config('portal.api_key'));
        $endpoint = "projectcategories.json";
        $options = [];
        $response = $portal->get($endpoint, $options);
        if (isset($response->categories)) {
            foreach($response->categories as $category) {
                $category = json_decode(json_encode($category), true);
                $categoryNew = new Category;
                $categoryNew->category_id = $category['id'];
                $categoryNew->type = $category['type'];
                $categoryNew->name = $category['name'];
                $categoryNew->parent_id = intVal($category['parent-id']);
                $categoryNew->save();
            }
        } else {
            $this->info('Failed to retrieve categories');
        }
        $this->info('Retrieved categories successfully');
    }
}
