<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\Project;
use App\Models\User;

use PortalAPIHelper;

class UpdateProjectHours extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:projectHours';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update hours of a project';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        \Log::info("Update Projects");
        
        $portal = new PortalAPIHelper(config('portal.api_key'));
        $projects = Project::all();
        $apiCallCount = 0; 
        foreach ($projects as $project) {
            $endpoint = "projects/api/v3/projects/".$project->project_id."/time/total.json";
            $this->info("Fetching hours for project ". $project->project_id);
            $response = $portal->get($endpoint);
            $apiCallCount++;
            $response = json_decode(json_encode($response), true);
            if (! empty($response['time-totals'])) {
                $project->update([
                    'estimated_hours' => $response['time-totals']['estimatedMinutes'] / 60, 
                    'total_hours' => $response['time-totals']['minutes'] / 60, 
                    'total_billable_hours' => $response['time-totals']['minutesBillable'] / 60, 
                    'total_non_billable_hours' => $response['time-totals']['minutesNonBillable'] / 60
                ]);
            }
            if ($apiCallCount >= 50) {
                $this->info("Sleeping for 1 min");
                sleep(60);
                $apiCallCount = 0;
            }
        } 
        $this->info("Project hours have been updated successfully");
    }
}
