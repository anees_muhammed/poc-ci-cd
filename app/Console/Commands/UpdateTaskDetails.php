<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Task;
use App\Models\TaskAssignee;
use App\Models\User;
use PortalAPIHelper;

class UpdateTaskDetails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:tasks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update details of tasks from portal';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        \Log::info('Update Task Details');
        $portal = new PortalAPIHelper(config('portal.api_key'));
        $this->info("Fetching data of existing tasks from portal...");
        $apiCallCount = 0; 
        $tasks = Task::where('status', '!=', 'completed')->get();
        foreach ($tasks as $k => $task) {
            $this->info('Updating task '.$task->task_id);
            $endpoint = "tasks/".$task->task_id."/time/total.json";
            $apiCallCount++;
            $response = $portal->get($endpoint);
            if (! empty($response->projects[0])) {
                $taskObj = Task::find($task->id);
                $taskObj->estimated_time_in_mins = $response->projects[0]->tasklist->task->{'time-estimates'}->{'total-mins-estimated'};
                $taskObj->time_spent_in_mins = $response->projects[0]->tasklist->task->{'time-totals'}->{'total-mins-sum'};
                $taskObj->progress = $taskObj->estimated_time_in_mins ? round(($taskObj->time_spent_in_mins / $taskObj->estimated_time_in_mins) * 100, 2) : 0;
                $taskObj->save();
            } else {
                $task->delete();
            }
            if ($apiCallCount >= 50) {
                $this->info("Sleeping for 1 min");
                sleep(60);
                $apiCallCount = 0;
            }
        }
        $this->info("Tasks fetched and updated successfully");
    }
}
