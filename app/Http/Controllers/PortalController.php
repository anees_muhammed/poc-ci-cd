<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\Task;
use Carbon\Carbon;
use Illuminate\Http\Request;
use PortalAPIHelper;
use Auth;

class PortalController extends Controller
{
    public function syncTasks()
    {
        $portal = new PortalAPIHelper(config('portal.api_key'));
        $endpoint = "projects/api/v2/projects/0/tasks.json";
        $tasks_filters = ["started", "late", "today", "upcoming"];

        foreach ($tasks_filters as $filter_type) {
            echo "Starting iteration for ". $filter_type . "<br>";
            $page = 1;
            do {
                echo "Fetching ". $filter_type . " type - starting page ". $page ." <br>";
                $options = $this->generateOptionsForTasks($filter_type, $page);
                $response = $portal->get($endpoint, $options);
                if (! empty($response->tasks)) {
                    foreach($response->tasks as $task) {
                        $task_arr = [
                            'task_id'    => $task->id,
                            'name'       => $task->name,
                            'projectId'  => $task->projectId,
                            'status'     => $task->status,
                            'priority'   => $task->priority,
                            'column'     => isset($task->boardColumn) ? $task->boardColumn->name : '-',
                            'due_date'   => $task->dueDate,
                            'tags'       => ! empty($task->tags) ? $task->tags : [],
                            'assignedTo' => ! empty($task->assignedTo) ? $task->assignedTo : [],
                        ];

                        $existingTask = Task::where('task_id', '=', $task->id)->first();
                        if ($existingTask) {
                            Task::where('task_id', $task->id)
                                ->update([
                                    'project_id' => $task->projectId,
                                    'name' => $task->name,
                                    'status' => $task->status,
                                    'priority' => $task->priority,
                                    'details' => $task_arr,
                                    'due_date' => ! empty($task_arr['due_date']) ? Carbon::parse($task_arr['due_date']) : null
                                ]);
                            echo "The task with ID: ".$task->id." has been updated <br>";
                        } else {
                            $task_new             = new Task;
                            $task_new->task_id    = $task->id;
                            $task_new->project_id = $task->projectId;
                            $task_new->name       = $task->name;
                            $task_new->status     = $task->status;
                            $task_new->priority   = $task->priority;
                            $task_new->details    = $task_arr;
                            $task_new->due_date   = ! empty($task_arr['due_date']) ? Carbon::parse($task_arr['due_date']) : null;
                            $task_new->save();
                            echo "The task with ID: ".$task->id." has been created <br>";
                        }
                    }
                    $page++;
                }
            }
            while(count($response->tasks) == 250);
        }
        echo "Task list has been updated successfully <br>";
    }

    public function syncProjects()
    {
        $portal = new PortalAPIHelper(config('portal.api_key'));
        $options = [
            "page"                               => 1,
            "pageSize"                           => "250",
            "getPermissions"                     => "true",
            "getNotificationSettings"            => "false",
            "getDateInfo"                        => "true",
            "searchCompany"                      => "true",
            "formatMarkdown"                     => "false",
            "getActivePages"                     => "true",
            "includeUpdates"                     => "true",
            "includeProjectOwner"                => "true",
            "include"                            => "budgets",
            "returnLetters"                      => "true",
            "orderBy"                            => "starredcompanyname",
            "orderMode"                          => "desc",
            "includeCustomFields"                => "true",
            // "firstLetter"                        => "",
            // "projectOwnerIds"                    => "",
            // "projectHealth"                      => "",
            "status"                             => "active",
            // "onlyStarredProjects"                => "false",
            // "hideObservedProjects"               => "false",
            // "onlyProjectsWithExplicitMembership" => "false",
        ];
        $endpoint = "projects/api/v2/projects.json";
        $response = $portal->get($endpoint, $options);
        if (! empty($response->projects)) {
            foreach($response->projects as $project) {
                $project_arr = [
                    'id' => $project->id,
                    'name' => $project->name,
                    'status' => $project->status,
                    'company' => $project->company,
                    'owner' => ! empty($project->owner) ? $project->owner : []
                ];

                $existingProject = Project::where('project_id', $project->id)->first();
                if (! $existingProject) {
                    $project_new = new Project;
                    $project_new->project_id = $project->id;
                    $project_new->name = $project->name;
                    $project_new->status = $project->status;
                    $project_new->details = $project_arr;
                    $project_new->owner = ! empty($project_arr['owner']) ? $project_arr['owner']->fullName : '-';
                    $project_new->save();
                    echo "The project with ID: ". $project_new->id . " has been created <br>";
                } else {
                    Project::where('project_id', $project->id)
                        ->update([
                            'name' => $project->name,
                            'status' => $project->status,
                            'details' => $project_arr,
                            'owner' => ! empty($project_arr['owner']) ? $project_arr['owner']->fullName : '-',
                        ]);
                    echo "The project with ID: ".$project->id." has been updated <br>";
                }
            }
        }
        echo "Project list has been updated successfully <br>";
    }


    private function generateOptionsForTasks($type, $page) {
        $options = [];
        // started
            // page=1
            // pageSize=50
            // include=taskListNames%2CprojectNames
            // filter=started
            // sort=startdate
            // sortOrder=desc
            // includeToday=1
            // useStartDatesForTodaysTasks=false
            // today=20210529
            // includeCustomFields=true
            // responsible-party-ids=236871
            // projectStatus=active
            // starredProjectsOnly=false
            // includeArchivedProjects=false
        //late
            // page=1
            // pageSize=50
            // include=taskListNames%2CprojectNames
            // filter=overdue
            // sort=startdate
            // sortOrder=desc
            // includeToday=1
            // useStartDatesForTodaysTasks=false
            // today=20210529
            // includeCustomFields=true
            // responsible-party-ids=236871
            // projectStatus=active
            // starredProjectsOnly=false
            // includeArchivedProjects=false
        // today
            // page=1
            // pageSize=50
            // include=taskListNames%2CprojectNames
            // filter=today
            // sort=startdate
            // sortOrder=desc
            // includeToday=1
            // useStartDatesForTodaysTasks=false
            // today=20210529
            // includeCustomFields=true
            // responsible-party-ids=236871
            // projectStatus=active
            // starredProjectsOnly=false
            // includeArchivedProjects=false

        //upcoming
            // page=1
            // pageSize=50
            // include=taskListNames%2CprojectNames
            // filter=within14
            // sort=startdate
            // sortOrder=desc
            // includeToday=0
            // useStartDatesForTodaysTasks=false
            // today=20210529
            // includeCustomFields=true
            // responsible-party-ids=236871
            // projectStatus=active
            // starredProjectsOnly=false
            // includeArchivedProjects=false

        $filter = 'started';
        switch($type)
        {
            case 'started':
                $filter = 'started';
                break;
            case 'late':
                $filter = 'overdue';
                break;
            case 'today':
                $filter = 'today';
                break;
            case 'upcoming':
                $filter = 'within14';
                break;
            default:
                $filter = 'all';
                break;
        }

        $options = [
            "page"                  => $page,
            "pageSize"              => 250,
            "filter"                => $filter,
            "sort"                  => "startdate",
            "sortOrder"             => "desc",
            "includeToday"          => 1,
            "today"                 => \Carbon\Carbon::now()->format("Ymd"),
            "includeCustomFields"   => true,
            "projectStatus"         => "active",
            "includeCompletedTasks" => false,
            "responsible-party-ids" => "364283",
            "includeTasksWithoutDueDates" => true,
            // "useStartDatesForTodaysTasks" => "false",
            // "starredProjectsOnly"         => "false",
            // "includeArchivedProjects"     => "false",
        ];
        return $options;
    }
}
