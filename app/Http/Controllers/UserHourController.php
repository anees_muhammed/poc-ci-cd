<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;

use App\Models\User;
use App\Models\Team;
use App\Models\UserHour;

class UserHourController extends Controller
{
    public function fetchHours(Request $request) {
        if ($request->date) {
            $q = Carbon::parse($request->date);
            if ($request->fetch_month == 'previous') {
                $date = $q->subMonth();
            } elseif ($request->fetch_month == 'next') {
                $date = $q->addMonth();
            } elseif ($request->fetch_month == 'current' && $request->sync_logs == 'true') {
                $date = $q; 
                \Artisan::call('fetch:timeLog', ['date' => $date]);
            }
        } else {
            $date = Carbon::now();
        }
        $data['date'] = Carbon::parse($date)->format('Y-m-d');
        $data['teams'] = Team::where('display_time_logs',1)->with(['users' => function ($q) use ($date) {
            $q->orderBy('name')->with(['hours' => function ($query) use ($date) {
                $query->orderBy('date_of_log')->whereBetween('date_of_log', [$date->startOfMonth()->format('Y-m-d'), $date->endOfMonth()->format('Y-m-d')]); 
            }]);
        }])->get();
        $data['previous'] = true;
        $data['next'] = ($date->diffInMonths(Carbon::now()) == 0 && $request->fetch_month != 'previous') ? false : true;
        return view('time-logs.listing', $data);
    }
}
