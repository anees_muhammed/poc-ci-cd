<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotifyTimeLogMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $teams, $date;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($teams, $date)
    {
        $this->teams = $teams;
        $this->date = $date;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('time-logs.mail')
            ->with([
                'teams' => $this->teams,
                'date' => $this->date,
            ]);
    }
}
