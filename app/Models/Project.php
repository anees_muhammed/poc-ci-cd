<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'details' => 'array',
    ];

    protected $fillable = ['project_id', 'name', 'status', 'owner', 'details', 'category_id', 'estimated_hours', 'total_hours', 'total_billable_hours', 'total_non_billable_hours', 'skip_updation'];

    public function tasks () {
        return $this->hasMany(Task::class, 'project_id', 'project_id');
    }

    public function category() {
        return $this->belongsTo(Category::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'project_tags');
    }
}
