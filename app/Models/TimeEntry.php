<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\TimeLogTag;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\SoftDeletes;

class TimeEntry extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'time_entry_id',
        'task_id',
        'user_id',
        'is_billable',
        'hours',
        'description',
        'date',
        'time_log_tag_id',
    ];

    public function task()
    {
        return $this->belongsTo(Task::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function timeLogTag() {
        return $this->belongsTo(TimeLogTag::class);
    }

    public static function boot()
    {
        parent::boot();
        static::saving(function ($model) {
            $tagId = 0;
            $tags = TimeLogTag::select('id', 'name')->get();
            foreach ($tags as $tag) {
                $contains = Str::contains($model->description, $tag->name);
                if ($contains) {
                    $tagId = $tag->id;
                    break;
                }
            }
            $model->time_log_tag_id = $tagId ?: TimeLogTag::where('name', DEV_TIME_LOG_TAG)->first()->id;
        });
    }
}
