<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserHour extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['user_id', 'billable_hours', 'non_billable_hours', 'date_of_log', 'total_hours'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    public function user() {
        return $this->belongsTo(User::class);
    }

    public static function boot()
    {
        parent::boot();
        static::saving(function ($model) {
            $billableHours = floatVal($model->billable_hours) ?: 0;
            $nonBillableHours = floatVal($model->non_billable_hours) ?: 0;
            $model->total_hours = $billableHours + $nonBillableHours;
        });
    }
}
