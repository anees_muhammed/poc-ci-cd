<?php

namespace App\Repositories;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use Carbon\Carbon;
//use Your Model

/**
 * Class GoogleCalendarRepository.
 */
class GoogleCalendarRepository extends BaseRepository
{
    private $clientId;
    private $clientSecret;
    private $googleCalBaseUrl;
    private $accessToken;
    private $totalPages;
    private $appUrl;
    private $apiKey;
    private $baseUrl;

    public function __construct () {
        $this->clientId = config('auth.google.oauth.client_id');
        $this->clientSecret = config('auth.google.oauth.client_secret');
        $this->googleCalBaseUrl = config('auth.google.oauth.base_url');
        $this->apiKey = config('auth.google.api_key');
        $this->apiUrl = config('auth.google.api_url');
        $this->appUrl = config('app.url');
        $this->baseUrl = config('auth.google.base_url');
    }

    /**
     * Gets an authorization code and redirects back to this project 
     *
     * @return $redirectUrl
     */
    public function requestAuthorizationCode () {
        $redirectUri = $this->appUrl.'/meetings';
        //Redirect url for Google Calendar
        $redirectUrl = $this->googleCalBaseUrl.'/auth?scope='.$this->baseUrl.'/auth/calendar&response_type=code&access_type=offline&redirect_uri='.$redirectUri.'&client_id='.$this->clientId;
        return $redirectUrl;
    }

    /**
     * Gets an access token using the authorization code
     * 
     * @param $authorizationCode
     *
     * @return $response
     */
    public function requestAccessToken ($authorizationCode) {
        $url = $this->googleCalBaseUrl.'/token';
        $redirectUri = $this->appUrl.'/meetings';
        $query = [
            'client_id' => $this->clientId,
            'client_secret' => $this->clientSecret,
            'redirect_uri' => $redirectUri,
            'code' => $authorizationCode,
            'grant_type' => 'authorization_code'
        ];
       
        $response = $this->curlPostRequest($query, $url);
        return $response;
    }

    public function refreshAccessToken ($refreshToken) {
        $url = $this->baseUrl.'/oauth2/v4/token';
        $query = [
            'client_id' => $this->clientId,
            'client_secret' => $this->clientSecret,
            'refresh_token' => $refreshToken,
            'grant_type' => 'refresh_token'
        ];
        $response = $this->curlPostRequest($query, $url);
        return $response;
    }

    public function listEvents ($accessToken, $email, $token) {
        $this->accessToken = $accessToken;
        $url = $this->apiUrl.'/'.urlencode($email).'/events?singleEvents=true&key='.$this->apiKey.'&pageToken='.$token.'&maxResults=1000&timeMin='.urlencode(Carbon::now()->startOfDay()->toRfc3339String());
        $events = $this->curlGetRequest($url);
        return $events;
    }

    /**
     * Passing cUrl Post requests
     * 
     * @param $query, $url
     *
     * @return $response
     */
    public function curlPostRequest ($query, $url) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POST => count($query),
            CURLOPT_POSTFIELDS => json_encode($query, JSON_UNESCAPED_SLASHES),
            CURLOPT_HTTPHEADER => array(
                "Content-type: application/json",
            ),
        ));
        
        $response = json_decode(curl_exec($curl));
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            Log::error($err);
            abort(401);
        }

        return $response;
    }

    /**
     * Passing cUrl Post requests
     * 
     * @param $url
     *
     * @return $response
     */
    public function curlGetRequest ($url) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Content-type: application/json",
                "Authorization: Bearer ".$this->accessToken
            ),
        ));
        
        $response = json_decode(curl_exec($curl));
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            Log::error($err);
            abort(401);
        }

        return $response;
    }
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        //return YourModel::class;
    }
}
