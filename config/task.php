<?php

return [

    'progress' => [
        'all' => [0,100],
        'first' => [0,25],
        'second' => [25,50],
        'third' => [50,100],
        'fourth' => [100,1000],
    ],



];
