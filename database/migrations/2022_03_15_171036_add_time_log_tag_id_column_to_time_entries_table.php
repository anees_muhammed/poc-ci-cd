<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTimeLogTagIdColumnToTimeEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('time_entries', function (Blueprint $table) {
            $table->unsignedBigInteger('time_log_tag_id')->nullable();
            $table->foreign('time_log_tag_id')->references('id')->on('time_log_tags')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('time_entries', function (Blueprint $table) {
            $table->dropForeign(['time_log_tag_id']);
        });

        Schema::table('time_entries', function (Blueprint $table) {
            $table->dropColumn('time_log_tag_id');
        });
    }
}
