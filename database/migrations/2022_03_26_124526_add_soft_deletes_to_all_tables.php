<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSoftDeletesToAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('comments', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('tags', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('tag_grouping_keys', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('task_assignees', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('task_tags', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('task_task_tag', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('teams', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('time_entries', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('time_log_tags', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('user_hours', function (Blueprint $table) {
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('comments', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });


        Schema::table('tags', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('tag_grouping_keys', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('task_assignees', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('task_tags', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('task_task_tag', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('teams', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('time_entries', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('time_log_tags', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('user_hours', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
}
