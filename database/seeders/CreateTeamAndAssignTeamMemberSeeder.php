<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use App\Models\Team;
use App\Models\User;

class CreateTeamAndAssignTeamMemberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('team_user')->truncate();
        DB::table('teams')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $devTeam = Team::create(['name' => 'Dev', 'display_time_logs' => True]);
        $devUsers = [
            'aathil.thadathil@whiterabbit.group',
            'abhay.nair@whiterabbit.group',
            'ajay.dev@whiterabbit.group',
            'akash.mohanan@whiterabbit.group',
            'alen.thomas@whiterabbit.group',
            'ambady.ajay@whiterabbit.group',
            'aparna.kiran@whiterabbit.group',
            'areeb.kareem@whiterabbit.group',
            'arun.george@whiterabbit.group',
            'arun.prakash@whiterabbit.group',
            'arun.thomas@whiterabbit.group',
            'arya.madhanagopal@whiterabbit.group',
            'bhavana.jagadeesh@whiterabbit.group',
            'binu.paul@whiterabbit.group',
            'casey.trauty@whiterabbit.group',
            'deepu.suresh@whiterabbit.group',
            'dona.xavier@whiterabbit.group',
            'fayisamol.p@whiterabbit.group',
            'george.thomas@whiterabbit.group',
            'jabinsha.j@whiterabbit.group',
            'jaison.john@whiterabbit.group',
            'jithu.jose@whiterabbit.group',
            'kiran.madathiparambil@whiterabbit.group',
            'kiran.varghese@whiterabbit.group',
            'liya.laiju@whiterabbit.group',
            'mujeeb@whiterabbit.group',
            'mukesh.jayaram@whiterabbit.group',
            'neel@whiterabbit.group',
            'nidhin.varghese@whiterabbit.group',
            'remya.toney@whiterabbit.group',
            'salga.thomas@whiterabbit.group',
            'sanoop.t@whiterabbit.group',
            'shaheer.ali@whiterabbit.group',
            'sharon.robert@whiterabbit.group',
            'sona@whiterabbit.group',
            'sreehari.pr@whiterabbit.group',
            'sumith@whiterabbit.group',
            'thanuja.palavalli@whiterabbit.group',
            'vaishnavi.baliga@whiterabbit.group',
            'vishnu.s@whiterabbit.group',
            'vivek@whiterabbit.group',
        ];
        foreach ($devUsers as $email) {
            $user = User::where('email', $email)->first();
            if ($user) {
                $user->teams()->attach($devTeam->id);
            }
        }

        $devOpsTeam = Team::create(['name' => 'DevOps', 'display_time_logs' => True]);
        $devOpsUsers = [
            'abu.alex@whiterabbit.group',
            'anees.muhammed@whiterabbit.group',
            'rahul.devakumar@whiterabbit.group',
            'ranjith@whiterabbit.group',

        ];
        foreach ($devOpsUsers as $email) {
            $user = User::where('email', $email)->first();
            if ($user) {
                $user->teams()->attach($devOpsTeam->id);
            }
        }

        $hrTeam = Team::create(['name' => 'HR', 'display_time_logs' => False]);
        $hrUsers = [
            'anusree@whiterabbit.group',
        ];
        foreach ($hrUsers as $email) {
            $user = User::where('email', $email)->first();
            if ($user) {
                $user->teams()->attach($hrTeam->id);
            }
        }

        $qaTeam = Team::create(['name' => 'QA', 'display_time_logs' => True]);
        $qaUsers = [
            'ajoy.varghese@whiterabbit.group',
            'alishya.sabu@whiterabbit.group',
            'arun.john@whiterabbit.group',
            'binchu.yohannan@whiterabbit.group',
            'michael.francis@whiterabbit.group',

        ];
        foreach ($qaUsers as $email) {
            $user = User::where('email', $email)->first();
            if ($user) {
                $user->teams()->attach($qaTeam->id);
            }
        }

        $managementTeam = Team::create(['name' => 'Management', 'display_time_logs' => False]);
        $managementUsers = [
            'abhilash@whiterabbit.group',
            'adam@whiterabbit.group',
            'greg@whiterabbit.group',
            'mark@whiterabbit.group',
        ];
        foreach ($managementUsers as $email) {
            $user = User::where('email', $email)->first();
            if ($user) {
                $user->teams()->attach($managementTeam->id);
            }
        }

        $dmTeam = Team::create(['name' => 'DM', 'display_time_logs' => False]);
        $dmUsers = [
            'divya.suseelan@whiterabbit.group',
            'kiran@whiterabbit.group',
        ];
        foreach ($dmUsers as $email) {
            $user = User::where('email', $email)->first();
            if ($user) {
                $user->teams()->attach($dmTeam->id);
            }
        }

        $pmTeam = Team::create(['name' => 'PM', 'display_time_logs' => False]);
        $pmUsers = [
            'camilo.luna@whiterabbit.group',
            'candace.bracht@whiterabbit.group',
            'hyuncheol.k@gmail.com',
            'marschal.bellinger@whiterabbit.group',
            'michelle.le@whiterabbit.group',
            'natalie.samaniego@whiterabbit.group',
            'paul.williamsen@whiterabbit.group',
        ];
        foreach ($pmUsers as $email) {
            $user = User::where('email', $email)->first();
            if ($user) {
                $user->teams()->attach($pmTeam->id);
            }
        }

        $generalTeam = Team::create(['name' => 'General', 'display_time_logs' => False]);
        $generalUsers = [
            'gina.naranjo@whiterabbit.group',
            'jithin@whiterabbit.group',
            'kristin@whiterabbit.group'
        ];
        foreach ($generalUsers as $email) {
            $user = User::where('email', $email)->first();
            if ($user) {
                $user->teams()->attach($generalTeam->id);
            }
        }


    }
}
