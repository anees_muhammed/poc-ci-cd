<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\TagGroupingKey;
use App\Models\Tag;

class UpdateTagOrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $groupingKeys = [
            [
                'name' => 'Retainer',
                'order' => 2,
            ],
            [
                'name' => 'AdHoc',
                'order' => 1,
            ],
            [
                'name' => 'FullBuild',
                'order' => 3,
            ],
        ];

        foreach($groupingKeys as $key) {
            $query = TagGroupingKey::where('name', $key['name']);
            ! $query->exists() ? TagGroupingKey::create(['name' => $key['name'], 'order' => $key['order']]) : $query->update(['order' => $key['order']]);
        }

        $retainerId = TagGroupingKey::where('name', 'Retainer')->first()->id;
        $adhocId = TagGroupingKey::where('name', 'AdHoc')->first()->id;
        $fullBuildId = TagGroupingKey::where('name', 'FullBuild')->first()->id;

        $tags = [
            [
                'name' => 'Retainer',
                'tag_grouping_key_id' => $retainerId,
            ],
            [
                'name' => 'Adhoc',
                'tag_grouping_key_id' => $adhocId,
            ],
            [
                'name' => 'Ad hoc',
                'tag_grouping_key_id' => $adhocId,
            ],
            [
                'name' => 'Internal',
                'tag_grouping_key_id' => $adhocId,
            ],
            [
                'name' => 'SMALL BILLABLE',
                'tag_grouping_key_id' => $adhocId,
            ],
            [
                'name' => 'HTML/CSS',
                'tag_grouping_key_id' => $adhocId,
            ],
            [
                'name' => 'OTP',
                'tag_grouping_key_id' => $fullBuildId,
            ],
            [
                'name' => 'Fixed Budget',
                'tag_grouping_key_id' => $fullBuildId,
            ],
        ];

        foreach ($tags as $tag) {
            Tag::where('name', $tag['name'])->update(['tag_grouping_key_id' => $tag['tag_grouping_key_id']]);
        }

    }
}
