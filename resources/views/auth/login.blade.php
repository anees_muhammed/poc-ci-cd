@extends('layouts.auth')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4 login_form">
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">Login</h3>
                </div>
                <!-- /.card-header -->
                <x-auth-validation-errors class="mb-4 alert alert-danger" :errors="$errors" />
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="email" class="col-sm-3 col-form-label">Email <span class="required"> * </span></label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{ old('email') }}" required autofocus>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-sm-3 col-form-label">Password <span class="required"> * </span></label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" name="password" required autocomplete="current-password" placeholder="Password">
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-info float-right">Sign in</button>
                    </div>
                    <!-- /.card-footer -->
                </form>
            </div>
        </div>
    </div>
</div><!-- /.container-fluid --> 
@endsection