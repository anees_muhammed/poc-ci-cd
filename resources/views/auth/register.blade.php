@extends('layouts.auth')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4 login_form">
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">Register</h3>
                </div>
                <!-- Validation Errors -->
                <x-auth-validation-errors class="mb-4 alert alert-danger" :errors="$errors" />
                <!-- /.card-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                    @csrf
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="name" class="col-sm-3 col-form-label">Name <span class="required"> * </span></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="{{ old('name') }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-sm-3 col-form-label">Username <span class="required"> * </span></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="username" id="username" placeholder="Username" value="{{ old('username') }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-sm-3 col-form-label">Email <span class="required"> * </span></label>
                            <div class="col-sm-9">
                                <input type="email" name="email" value="{{ old('email') }}" required class="form-control" id="email" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-sm-3 col-form-label">Password <span class="required"> * </span></label>
                            <div class="col-sm-9">
                                <input type="password" name="password" class="form-control" id="password" placeholder="Password" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password_confirmation" class="col-sm-3 col-form-label">Confirm Password <span class="required"> * </span></label>
                            <div class="col-sm-9">
                                <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Confirm password" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="apikey" class="col-sm-3 col-form-label">Portal API Key <span class="required"> * </span></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="apikey" id="apikey" placeholder="API Key" value="{{ old('apikey') }}" required>
                            </div>
                        </div>
                        
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-info">Submit</button>
                        <a href="{{ route('login') }}"><button type="button" class="btn btn-default float-right">Login</button></a>
                    </div>
                    <!-- /.card-footer -->
                </form>
            </div>
        </div>
    </div>
</div><!-- /.container-fluid --> 
@endsection