@extends('layouts.app')

@section('content')
<div class="projects-section" id="projects-block">
    <div class="projects-section-header projects-section-header_inline">
        <p class="project-section-header__title">Tasks</p>
        <form action="{{ route('dashboard') }}" method="get" id="copy_tasks">
            {{ csrf_field() }}
            <input type="hidden" name="button_click" value="true">
            <button class="export__button">Copy Tasks</button>
        </form>
        <p class="time dashboard__time"><?php echo Carbon\Carbon::now()->format('M, d') ?></p>
    </div>
    <form action="{{ route('dashboard') }}" method="get">
        {{ csrf_field() }}
        <div class="row" style="margin-bottom: 20px;">
            <select name="projects" id="projects" class="dropdown" style="width: 10%; margin-right: 20px; background: #c8c8c8">
                <option value="0">&nbsp;&nbsp;All Projects</option>
                @foreach ($projects as $project)
                    <option value="{{ $project->id }}" {{ $filters->projects && $filters->projects == $project->id ? "selected" : ""  }}>{{ $project->name }}</option>
                @endforeach
            </select>
            <select name="task_progress" id="task_progress" class="dropdown" style="width: 15%; margin-right: 20px; background: #c8c8c8">
                <option value="all">&nbsp;&nbsp;Task Progress</option>
                <option value="first" {{ $filters->task_progress && $filters->task_progress == "first" ? "selected" : ""  }}>0-25%</option>
                <option value="second" {{ $filters->task_progress && $filters->task_progress == "second" ? "selected" : ""  }}>25-50%</option>
                <option value="third" {{ $filters->task_progress && $filters->task_progress == "third" ? "selected" : ""  }}>50-100%</option>
                <option value="fourth" {{ $filters->task_progress && $filters->task_progress == "fourth" ? "selected" : ""  }}>>100%</option>
            </select>
            <select name="due_date" id="due_date" class="dropdown" style="width: 10%; margin-right: 20px; background: #c8c8c8">
                <option value="none">&nbsp;&nbsp;Due Date</option>
                <option value="today" {{ $filters->due_date && $filters->due_date == "today" ? "selected" : ""  }}>Today</option>
                <option value="tomorrow" {{ $filters->due_date && $filters->due_date == "tomorrow" ? "selected" : ""  }}>Tomorrow</option>
                <option value="next_week" {{ $filters->due_date && $filters->due_date == "next_week" ? "selected" : ""  }}>Next Week</option>
                <option value="next_month" {{ $filters->due_date && $filters->due_date == "next_month" ? "selected" : ""  }}>Next Month</option>
            </select>
            <select name="project_tag" id="project_tag" class="dropdown" style="width: 10%; margin-right: 5px; background: #c8c8c8">
                <option value="none">&nbsp;&nbsp;Project Tag</option>
                @foreach ($tags as $tag) 
                    <option value="{{ $tag->tag_id }}" {{ $filters->project_tag && $filters->project_tag == $tag->tag_id ? "selected" : ""  }}>{{ $tag->name }}</option>
                @endforeach
            </select>
            <label for="start_date" class="date_label" style="margin-left: 10px; width: 4%">Start Date: &nbsp;</label>
            <input type="date" class="dropdown" id="start_date" name="start_date" style="width: 12%; margin-right: 20px; background: #c8c8c8" value="{{ $filters->start_date ? $filters->start_date : '' }}">
            <label for="end_date" class="date_label" style="width: 4%">End Date: &nbsp;</label>
            <input type="date" class="dropdown" id="end_date" name="end_date" style="width: 12%; margin-right:20px; background: #c8c8c8" value="{{ $filters->end_date ? $filters->end_date : '' }}">
            <button class="update_filters__button" style="width: 12%; margin-right: 10px">Update filters</button>
            <br>
        </div>
    </form>
    @include('partial.task_table')
</div>
<div class="modal fade" id="modelWindow" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document" style="max-width: 700px">
    <div class="modal-content" style="width: 700px">
      <div class="modal-header">
        <h5 class="modal-title">To Do</h5>
        <button type="button" id="to_do__close" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="editor">{!! $html ?? '' !!}</div>
      </div>
    </div>
  </div>
</div>
<script>
    $(document).ready(function() {
        let button_click = false;
        let pass_html = false;
        $('#modelWindow').modal('hide');
        function fixDates() {
            let due_date = $('#due_date').val();
            if (due_date != 'none') {
                $('#start_date').val('');
                $('#end_date').val('');
            } 
        }

        $('#to_do__close').on('click', function(e) {
            $('#modelWindow').modal('hide');
        });

        $('.export__button').on('click', function(e) {
            e.preventDefault();
            $('#modelWindow').modal('show');
        });

        $('.sync__button').on('click', function() {
            $(".sync__button").text('Syncing...');
            $.ajax({
                type: "POST",
                url: "/sync-tasks",
                data: {
                    "_token": "{{ csrf_token() }}"
                },
                global: false,
                async:false,
                success: function(data) {
                    $(".sync__button").text('Sync Tasks');
                }
            });
            let button_click = false;
            updateTaskListing(button_click);
        });

        $('update_filters__button').click(function(e){ 
            e.preventDefault();
            fixDates();
            this.form.submit();
        });

    });
</script>
<script src="https://cdn.ckeditor.com/ckeditor5/32.0.0/classic/ckeditor.js"></script>
<script>
    ClassicEditor
    .create( document.querySelector( '#editor' ) )
    .then( editor => {
            console.log( editor );
    } )
    .catch( error => {
            console.error( error );
    } );
</script>
@endsection
