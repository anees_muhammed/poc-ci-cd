
<nav>
  <div class="nav nav-tabs" id="nav-tab" role="tablist">
    @foreach ($tag_grouping_keys as $c => $key)
      <button class="nav-link {{ ! $c ? 'active' : ''}}" data-bs-toggle="tab" id="nav-{{ strtolower(str_replace(' ', '', $key->name)) }}-tab" data-bs-target="#nav-{{ strtolower(str_replace(' ', '', $key->name)) }}" type="button" role="tab" aria-controls="nav-{{ strtolower(str_replace(' ', '', $key->name)) }}" aria-selected="{{ ! $c ? 'true' : 'false' }}">{{ $key->name }}</button>
    @endforeach
  </div>
</nav>
<div class="task_listing__scroll">
  <div class="tab-content" id="nav-tabContent">
    @foreach ($tag_grouping_keys as $c => $key)
      <div class="tab-pane fade show {{ ! $c ? 'active' : ''}}" id="nav-{{ strtolower(str_replace(' ', '', $key->name)) }}" role="tabpanel" aria-labelledby="nav-{{ strtolower(str_replace(' ', '', $key->name)) }}-tab">
        @if (isset($key->projects))
          @foreach ($key->tags as $tag)
            @foreach ($tag->projects as $project)
              @if (isset($project->tasks))
                @php $addProjectTitle = false; @endphp
                @foreach ($project->tasks as $task)
                    @if (! $addProjectTitle)
                      <table class="table table-bordered" style="border-collapse:collapse;">
                        <tbody>
                          <tr colspan="7" class="table__project_title">
                            <td colspan="7">{{ $project->name }}</td>
                          </tr>
                          <tr class="table__tab_column">
                            <td style="width: 35%">Task Name</td>
                            <td style="width: 10%">Due Date</td>
                            <td style="width: 7%">Time Spent</td>
                            <td style="width: 7%">Estimated Time</td>
                            <td style="width: 10%">Last Commented On</td>
                            <td style="width: 10%">Last Modified On</td>
                            <td style="width: 10%">Info</td>
                          </tr>
                          @php $addProjectTitle = true; @endphp
                        @endif
                        <tr colspan="5" data-toggle="collapse" data-target="#task{{ $task->task_id }}" class="accordion-toggle table__project_tasks">
                          <td>
                            <a href="https://portal.whiterabbit.group/#/tasks/{{ $task->task_id }}" class="task__link" target="_blank">
                              {{ $task->name }} 
                              @if ($task->taskTags->contains('name', 'Estimate First')) 
                                (Estimate)
                              @endif
                            </a>
                          </td>
                          <td class="table_center_text">
                            @if ($task->due_date)
                              Due {{ $task->taskDate }} 
                            @else
                              No Due Date
                            @endif
                          </td>
                          <td class="table_center_text">{{ round($task->time_spent_in_mins / 60, 2) }}</td>
                          <td class="table_center_text">{{ round($task->estimated_time_in_mins / 60, 2) }}</td>
                          <td class="table_center_text">{{ count($task->comments) > 0 ? Carbon\Carbon::parse($task->comments->max('datetime'))->format('M j, Y') : '-' }}</td>
                          <td class="table_center_text">{{ Carbon\Carbon::parse($task->last_modified_at)->format('M j, Y') ?? '-' }}</td>
                          <td class="table_center_text">
                            <p class="custom_row__due_date">
                              <span class="fa-stack fa-1x">
                                @if ($task->taskPriority == 'Low')
                                  <a class="info_tag" title="Low Prio">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="#35e735" class="bi bi-exclamation-circle-fill" viewBox="0 0 16 16">
                                      <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8 4a.905.905 0 0 0-.9.995l.35 3.507a.552.552 0 0 0 1.1 0l.35-3.507A.905.905 0 0 0 8 4zm.002 6a1 1 0 1 0 0 2 1 1 0 0 0 0-2z"/>
                                    </svg>
                                  </a>
                                @elseif ($task->taskPriority == 'Medium')
                                  <a class="info_tag" title="Medium Prio">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="#FFFF33" class="bi bi-exclamation-circle-fill" viewBox="0 0 16 16">
                                      <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8 4a.905.905 0 0 0-.9.995l.35 3.507a.552.552 0 0 0 1.1 0l.35-3.507A.905.905 0 0 0 8 4zm.002 6a1 1 0 1 0 0 2 1 1 0 0 0 0-2z"/>
                                    </svg>
                                  </a>
                                @elseif ($task->taskPriority == 'High')
                                  <a class="info_tag" title="High Prio">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="#F00000" class="bi bi-exclamation-circle-fill" viewBox="0 0 16 16">
                                      <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8 4a.905.905 0 0 0-.9.995l.35 3.507a.552.552 0 0 0 1.1 0l.35-3.507A.905.905 0 0 0 8 4zm.002 6a1 1 0 1 0 0 2 1 1 0 0 0 0-2z"/>
                                    </svg>
                                  </a>
                                @endif
                              </span>
                              <span class="fa-stack fa-1x">
                                <a class="info_tag" title="{{ ($task->progress > 100) ? 'High Spent Time' : (($task->progress <=100 && $task->progress >= 90) ? 'Medium Spent Time' : 'Low Spent Time') }}">
                                  <i class="fas fa-user-clock {{ ($task->progress > 100) ? 'high_time' : (($task->progress <=100 && $task->progress >= 90) ? 'medium_time' : 'low_time') }}"></i>
                                </a>
                              </span>
                              @if (count($task->comments) > 0)
                                <span class="fa-stack fa-1x info_tag" title="View Latest Comments" data-count="{{ count($task->comments) }}">
                                  <i class="fas fa-comment fa-stack-1x"></i>
                                </span>
                              @endif
                            </p>
                          </td>
                        </tr>
                        @if (count($task->comments) > 0)
                          <tr class="p table__task_comments">
                            <td colspan="7" class="hiddenRow">
                              <div class="accordian-body collapse p-3" id="task{{ $task->task_id }}">
                                <div class="custom_row__content">
                                    @foreach ($task->comments as $comment)
                                        <div class="container comment_box">
                                            <div class="row">
                                                <div class="col-2 date__comment">
                                                    <img src="{{ $comment->user->avatar }}" style="width: 90px"/>
                                                </div>
                                                <div class="col-7">
                                                    <a href="{{ config('portal.api_url') }}/#/tasks/{{ $task->task_id }}?c={{ $comment->comment_id }}" target="_blank"><p>{!! $comment->body !!}</p></a> 
                                                </div>
                                                <div class="col-3 date__comment">
                                                    <b>{{ \Carbon\Carbon::parse($comment->datetime)->format('j M, h:i A') }}</b>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                              </div>
                            </td>
                          </tr>
                        @endif
                      </tbody>
                    @endforeach
                </table>
              @endif
            @endforeach
          @endforeach
        @endif
      </div>
    @endforeach
  </div>
</div>

<script>
  $('.accordion-toggle').click(function(){
    $('.hiddenRow').hide();
    $(this).next('tr').find('.hiddenRow').show();
  });
</script>