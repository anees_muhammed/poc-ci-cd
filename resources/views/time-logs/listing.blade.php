@extends('layouts.app')

@section('content')
<div class="projects-section custom-background" id="projects-block">
    <div class="row">
        <h3 class="project-section-header__title time_log__title">Time Logs - {{ Carbon\Carbon::parse($date)->format('M Y') }}</h3>
        <form action="userHours" id="time-logs-form">
            <button id="previous-button" class="update_filters__button time_log__button" style="display:inline-block;"><< Previous</button>
            @if ($next)
                <button id="next-button" class="update_filters__button time_log__button" style="display:inline-block;">Next >></button>
            @endif
            <button id="sync-logs" class="update_filters__button time_log__button" style="display:inline-block;">Sync Logs</button>
        </form>
    </div>
    <br>
    @foreach ($teams as $team)
        <div class="row">
            <h2 class="project-section-header__title time_log__title">{{ $team->name }}</h2>
        </div>
        <table class="table" style="border-collapse:collapse; margin: 20px 0;">
        <tbody>
            <tr class="table__project_title">
                <td>Name</td>
                @for ($i=1; $i <= Carbon\Carbon::now()->daysInMonth; $i++) 
                    <td>{{ $i }}</td>
                @endfor
            </tr>
            @foreach ($team->users as $user)
                <tr class="table__project_tasks">
                    <td>{{ $user->name }}</td>
                    @php $j=1; @endphp
                    @foreach ($user->hours as $hour)
                        @php $check=false; @endphp
                        @while ($check == false)
                            @if ($j == Carbon\Carbon::parse($hour->date_of_log)->format('j'))
                                <td style="background: {{ floatVal($hour->total_hours) >=6 ? 'green' : 'red' }}">{{ floatVal($hour->total_hours) }}</td>
                                @php $check=true; @endphp
                            @else
                                <td>-</td>
                                @php $check=false; @endphp
                            @endif
                            @php $j++; @endphp
                        @endwhile
                    @endforeach
                    @for ($k = 0; $k <= (Carbon\Carbon::now()->daysInMonth - $j); $k++)
                        <td>-</td>
                    @endfor
                </tr>
            @endforeach
        </tbody>
    </table>
    <br>
    @endforeach
</div>

<script>
    $("#previous-button").on('click', function() {  
        let date = $("<input>").attr("type", "hidden").attr("name", "date").val("{{ $date }}");
        let fetch_month = $("<input>").attr("type", "hidden").attr("name", "fetch_month").val("previous");
        let sync_logs = $("<input>").attr("type", "hidden").attr("name", "sync_logs").val("false");
        $('#time-logs-form').append(date).append(fetch_month).append(sync_logs);
    });

    $("#next-button").on('click', function() {  
        let date = $("<input>").attr("type", "hidden").attr("name", "date").val("{{ $date }}");
        let fetch_month = $("<input>").attr("type", "hidden").attr("name", "fetch_month").val("next");
        let sync_logs = $("<input>").attr("type", "hidden").attr("name", "sync_logs").val("false");
        $('#time-logs-form').append(date).append(fetch_month).append(sync_logs);
    });

    $("#sync-logs").on('click', function() {  
        let date = $("<input>").attr("type", "hidden").attr("name", "date").val("{{ $date }}");
        let fetch_month = $("<input>").attr("type", "hidden").attr("name", "fetch_month").val("current");
        let sync_logs = $("<input>").attr("type", "hidden").attr("name", "sync_logs").val("true");
        $('#time-logs-form').append(date).append(fetch_month).append(sync_logs);
    });
    
</script>
@endsection
