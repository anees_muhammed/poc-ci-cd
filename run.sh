# Turn on maintenance mode
php artisan down

# Pull code changes
git pull

#update packages
composer install

#refresh autload files
composer dump-autoload

#clear aritsan cache
php artisan cache:clear

#clear config cache
php artisan config:clear

# Refresh migrations & seed data
php artisan migrate

# Fetch all WRG Employees
php artisan sync:users

# Fetch all Project Categories
php artisan sync:projectCategories 

# Fetch all Projects
php artisan sync:projects

# Fetch all Project Hours
php artisan update:projectHours

# Sync tasks of all projects
php artisan sync:tasks

# Fetch more details of each task
php artisan update:tasks

# Sync time of tasks
php artisan sync:time

# Fetch comments posted in the past 24 hrs
php artisan sync:latestComments

# Turn off maintenance mode
php artisan up